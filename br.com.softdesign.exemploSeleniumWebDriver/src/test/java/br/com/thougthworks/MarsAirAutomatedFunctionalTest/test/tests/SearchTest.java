package br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.dataProviders.SearchDataProvider;
import br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.pages.interactions.ResultPageInteractions;
import br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.pages.interactions.SearchPageInteractions;
import br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.utils.DriverTestIntance;

public class SearchTest extends DriverTestIntance{

	//initialize data provider with test
    @Test(dataProvider = "DadosDeTestedeIntervalosComdisponibilidadeParaIrAMarte", dataProviderClass=SearchDataProvider.class)
    public void searchAvailableAviability(String dtDeparting, String dtReturning) throws Exception {
    	
    	// Create a new instance of the index page class
        // and initialize any WebElement fields in it.
        SearchPageInteractions searchPageInteractor = PageFactory.initElements(driver, SearchPageInteractions.class);
        
        driver.get(baseUrl+"/");
        // And now do the search.
        searchPageInteractor.setSelectDeparting(driver,dtDeparting);
        searchPageInteractor.setSelectReturning(driver,dtReturning);
        searchPageInteractor.actionSearch();;
        // Initialize the result
        ResultPageInteractions resultPageInteractor = PageFactory.initElements(driver, ResultPageInteractions.class);
        // Assert results avaliable
        resultPageInteractor.assertDatesAvaliable();
        // Execute return Search page
        resultPageInteractor.actionBackSearchPage();
        
    }
    
  //initialize data provider with test
    @Test(dataProvider = "DadosDeTestedeIntervalosSemdisponibilidadeParaIrAMarte", dataProviderClass=SearchDataProvider.class)
    public void searchNotAvailableAviability(String dtDeparting, String dtReturning) throws Exception {
    	
    	// Create a new instance of the index page class
        // and initialize any WebElement fields in it.
        SearchPageInteractions searchPageInteractor = PageFactory.initElements(driver, SearchPageInteractions.class);
        
        // And now do the search.
        driver.get(baseUrl+"/");
        searchPageInteractor.setSelectDeparting(driver,dtDeparting);
        searchPageInteractor.setSelectReturning(driver,dtReturning);
        searchPageInteractor.actionSearch();;
        // Initialize the result
        ResultPageInteractions resultPageInteractor = PageFactory.initElements(driver, ResultPageInteractions.class);
        // Assert results not avaliable
        resultPageInteractor.assertDatesNotAvaliable();
        // Execute return Search page
        resultPageInteractor.actionBackSearchPage();
        
    }
    
    //initialize data provider with test
    @Test(dataProvider = "DadosDeTestedeIntervalosIvalidos", dataProviderClass=SearchDataProvider.class)
    public void searchInvalidSchedule(String dtDeparting, String dtReturning) throws Exception {
    	
    	// Create a new instance of the index page class
        // and initialize any WebElement fields in it.
        SearchPageInteractions searchPageInteractor = PageFactory.initElements(driver, SearchPageInteractions.class);
        
        // And now do the search.
        driver.get(baseUrl+"/");
        searchPageInteractor.setSelectDeparting(driver,dtDeparting);
        searchPageInteractor.setSelectReturning(driver,dtReturning);
        searchPageInteractor.actionSearch();;
        // Initialize the result
        ResultPageInteractions resultPageInteractor = PageFactory.initElements(driver, ResultPageInteractions.class);
        // Assert schedule invalid
        resultPageInteractor.assertScheduleInvalid();
        // Execute return Search page
        resultPageInteractor.actionBackSearchPage();
        
    }
    
  //initialize data provider with test
    @Test(dataProvider = "DadosDeTestedePromocodeValido", dataProviderClass=SearchDataProvider.class)
    public void searchWithPromocodeValid(String dtDeparting, String dtReturning, String promocode, String desc) throws Exception {
    	
    	// Create a new instance of the index page class
        // and initialize any WebElement fields in it.
        SearchPageInteractions searchPageInteractor = PageFactory.initElements(driver, SearchPageInteractions.class);
        
        // And now do the search.
        driver.get(baseUrl+"/");
        searchPageInteractor.setSelectDeparting(driver,dtDeparting);
        searchPageInteractor.setSelectReturning(driver,dtReturning);
        searchPageInteractor.keyPromoCode(promocode);
        searchPageInteractor.actionSearch();;
        // Initialize the result
        ResultPageInteractions resultPageInteractor = PageFactory.initElements(driver, ResultPageInteractions.class);
        // Assert promo code valid
        resultPageInteractor.assertPromoCodeValid(promocode, desc);
        // Execute return Search page
        resultPageInteractor.actionBackSearchPage();
        
    }

  //initialize data provider with test
    @Test(dataProvider = "DadosDeTestedePromocodeInvalido", dataProviderClass=SearchDataProvider.class)
    public void searchWithPromocodeInvalid(String dtDeparting, String dtReturning, String promocode) throws Exception {
    	
    	// Create a new instance of the index page class
        // and initialize any WebElement fields in it.
        SearchPageInteractions searchPageInteractor = PageFactory.initElements(driver, SearchPageInteractions.class);
        
        // And now do the search.
        driver.get(baseUrl+"/");
        searchPageInteractor.setSelectDeparting(driver,dtDeparting);
        searchPageInteractor.setSelectReturning(driver,dtReturning);
        searchPageInteractor.keyPromoCode(promocode);
        searchPageInteractor.actionSearch();;
        // Initialize the result
        ResultPageInteractions resultPageInteractor = PageFactory.initElements(driver, ResultPageInteractions.class);
        // Assert promo code invalid
        resultPageInteractor.assertPromoCodeIvalid(promocode);
        // Execute return Search page
        resultPageInteractor.actionBackSearchPage();
        
    }

}