package br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchPage{

	@FindBy(id = "departing")
	protected WebElement selectDeparting;
	
	@FindBy(id = "returning")
	protected WebElement selectReturning;

	@FindBy(name = "promotional_code")
	protected WebElement textPromotional_code;
	
	@FindBy(css = "input[type=\'submit\']")
	protected WebElement btnSearch;

}