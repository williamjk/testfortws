package br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class DriverTestIntance{

    // Create a new instance of a driver
    public WebDriver driver = new FirefoxDriver();
    protected String baseUrl = "http://williamjablonski.marsair.tw/";
    
    @BeforeClass
    public void setUp() {
        // Navigate to the right place
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseUrl);
    }

     @AfterClass
     public void tearDown(){
         // Close the browser
    	 driver.close();
         driver.quit();
     }

}