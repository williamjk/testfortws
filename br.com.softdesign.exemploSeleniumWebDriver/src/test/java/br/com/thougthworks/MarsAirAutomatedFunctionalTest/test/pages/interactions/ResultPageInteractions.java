package br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.pages.interactions;

import org.testng.Assert;

import br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.pages.ResultPage;

public class ResultPageInteractions extends ResultPage{
   
    
    public void actionBackSearchPage() {
    	linkBack.click();
    }

    public void assertDatesAvaliable() {
    	Assert.assertEquals(textCallNow.getText(),"Call now on 0800 MARSAIR to book!");
    }
    
    public void assertDatesNotAvaliable() {
    	Assert.assertEquals(textSorryNoMoreSetsAvaliable.getText(),"Sorry, there are no more seats available.");
    }
    
    public void assertScheduleInvalid() {
    	Assert.assertEquals(textScheduleIvalid.getText(),"Unfortunately, this schedule is not possible. Please try again.");
    }
    
    public void assertPromoCodeValid(String code, String desc) {
    	Assert.assertEquals(textTruePromotionaCode.getText(),"Promotional code "+code+" used: "+desc+"0% discount!");
    } 
    
    public void assertPromoCodeIvalid(String code) {
    	Assert.assertEquals(textFalsePromotionaCode.getText(),"Sorry, code "+code+" is not valid");
    } 
    
    
}