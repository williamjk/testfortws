package br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.dataProviders;

import org.testng.annotations.DataProvider;

public class SearchDataProvider {
	
	//This method will provide data to any test method that declares that its Data Provider
		@DataProvider(name="DadosDeTestedeIntervalosComdisponibilidadeParaIrAMarte")
		public static Object[][] createIntervalAvailable() {
		 return new Object[][] {
		   { "0", "5" }, 
		 };
		};
		
	//This method will provide data to any test method that declares that its Data Provider		
		@DataProvider(name="DadosDeTestedeIntervalosSemdisponibilidadeParaIrAMarte")
		public static Object[][] createIntervalNotAvailable() {
		 return new Object[][] {
		   { "0", "2" },
		 };
		};
	
		//This method will provide data to any test method that declares that its Data Provider		
		@DataProvider(name="DadosDeTestedeIntervalosIvalidos")
		public static Object[][] createIntervalIvalid() {
		return new Object[][] {
		   { "0", "0" },
		   { "1", "2" },
		 };
		};
		
		//This method will provide data to any test method that declares that its Data Provider		
		@DataProvider(name="DadosDeTestedePromocodeValido")
		public static Object[][] createPromocodeValid() {
		return new Object[][] {
		   { "0", "5", "AF3-FJK-418", "3" },
		   { "0", "5", "AF7-FJK-119", "7" },
		   { "0", "5", "JJ5-OPQ-320", "5" },
		   { "0", "5", "FF9-MNO-997", "9"},
		 };
		};
		
		//This method will provide data to any test method that declares that its Data Provider		
		@DataProvider(name="DadosDeTestedePromocodeInvalido")
		public static Object[][] createPromocodeInvalid() {
		return new Object[][] {
		  { "0", "5", "FF2-MNO-5181sdfghjtyu34567" }, 
		  { "0", "5", "AF3-FJK-415" },
		  { "0", "5", "AFA-FJK-415" },
		 };
		};
		
		
	
}
