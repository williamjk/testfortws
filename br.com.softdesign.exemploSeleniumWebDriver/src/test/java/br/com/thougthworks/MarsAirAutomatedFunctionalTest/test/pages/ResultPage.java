package br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultPage {

    @FindBy(xpath = "//div[@id='content']/p[2]")
	protected WebElement textCallNow;
    //"Call now on 0800 MARSAIR to book!"
    
    @FindBy(css = "p")
    protected WebElement textSorryNoMoreSetsAvaliable;
    //"Sorry, there are no more seats available."
    
    @FindBy(css = "p.promo_code")
    protected WebElement textTruePromotionaCode;
    //"Promotional code"
  
    @FindBy(css = "p.promo_code")
    protected WebElement textFalsePromotionaCode;
    //"Sorry, code "
    
    @FindBy(css = "p")
    protected WebElement textScheduleIvalid;
    //"Unfortunately, this schedule is not possible. Please try again."
    
    @FindBy(linkText = "Back")
    protected WebElement linkBack;
    
}