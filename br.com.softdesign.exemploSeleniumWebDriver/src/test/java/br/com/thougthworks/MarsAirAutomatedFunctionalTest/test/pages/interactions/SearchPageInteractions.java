package br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.pages.interactions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import br.com.thougthworks.MarsAirAutomatedFunctionalTest.test.pages.SearchPage;

public class SearchPageInteractions extends SearchPage{

    public void setSelectDeparting(WebDriver driver, String dtDeparting) {
         
    	(new Actions(driver)).moveToElement(selectDeparting).perform();
		new Select(selectDeparting).selectByValue(dtDeparting);
    }
    
    public void setSelectReturning(WebDriver driver, String dtReturning) {
        
      	(new Actions(driver)).moveToElement(selectReturning).perform();
		new Select(selectReturning).selectByValue(dtReturning);
    }
    
    public void keyPromoCode(String promocode) {
        
        textPromotional_code.sendKeys(promocode);
    }
    
    public void actionSearch() {
     
    	btnSearch.click();
    }

}